<?
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Sum\cMainTest;

CModule::IncludeModule("sum.main");
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">ID клиента</th>
        <th scope="col">Название клиента</th>
        <th scope="col">Сумма по задачам в статусе "Выполнено"</th>
        <th scope="col">Сумма по задачам в статусе "В процессе"</th>
        <th scope="col">Общее количество задач клиента</th>
    </tr>
    </thead>
    <tbody>
    <? foreach (cMainTest::getData() as $NAME => $DATA): ?>
        <tr>
            <th scope="row"><?= $DATA['ID'] ?></th>
            <td><?= $NAME ?></td>
            <td><?= $DATA['FINISH'] ?></td>
            <td><?= $DATA['PENDING'] ?></td>
            <td><?= $DATA['COUNT_TASK'] ?></td>
        </tr>
    <? endforeach; ?>
    </tbody>
</table>