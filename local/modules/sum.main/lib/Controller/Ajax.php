<?php

namespace Sum\Controller;

use Sum\cMainTest;
use Bitrix\Main\Engine\Controller as EngineController;

class Ajax extends EngineController implements \Bitrix\Main\Engine\Contract\Controllerable
{

    public function configureActions()
    {
        return [
            'getSum' => [
                'prefilters' => []
            ]
        ];
    }

    public function getSumAction()
    {
        return cMainTest::getData();
    }
}
