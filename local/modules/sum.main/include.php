<?php

use Bitrix\Main\Loader;

$arClasses = array(
    'Sum\\cMainTest' => 'classes/general/cMainTest.php',
    'Sum\\Controller\\Ajax' => 'lib/Controller/Ajax.php',
);

Loader::registerAutoLoadClasses('sum.main', $arClasses);
