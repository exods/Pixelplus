<?php

namespace Sum;

class cMainTest
{
    static $MODULE_ID = "sum.test";


    static function getData(): array
    {
        $status = self::getTasksStatus();

        $users = self::getUsers();
        $tasks = self::getTasks();

        foreach ($tasks as $idUser => $task) {
            $data[$users[$idUser]] = $task;
            $data[$users[$idUser]]['ID'] = $idUser;
        }
        uasort($data, function ($a, $b) {
            return $a['ID'] - $b['ID'];
        });
        return $data;
    }

    private function getUsers(): array
    {
        $users = [];
        $results = self::queryDB("SELECT * FROM px_client");
        while ($row = $results->Fetch()) {
            $users[$row['ID']] = $row['NAME'];
        }
        return $users;
    }

    private function getTasks(): array
    {
        $status = self::getTasksStatus();
        $tasks = [];
        $results = self::queryDB("SELECT * FROM px_task");
        while ($row = $results->Fetch()) {
            $tasks[$row['CLIENT_ID']][$status[$row['STATUS_ID']]] += $row['PRICE'];
            $tasks[$row['CLIENT_ID']]['COUNT_TASK'] += 1;
        }
        return $tasks;
    }

    private function getTasksStatus(): array
    {
        $arrStatus = [1 => 'PENDING', 2 => 'FINISH'];
        $statusList =[];
        $results = self::queryDB("SELECT * FROM px_task_status");
        while ($row = $results->Fetch()) {
            $statusList[$row['ID']] = $arrStatus[$row['ID']];
        }
        return $statusList;
    }

    private function queryDB(string $sql)
    {
        global $DB;
        return $DB->Query($sql);
    }

    static function debug($ar)
    {
        echo '<pre>';
        print_r($ar);
        echo '</pre>';
    }

}