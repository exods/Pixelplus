<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Sum\\Controller' => 'api',
                'restIntegration' => [
                    'enabled' => true,
                ],
            ],
        ],
        'readonly' => true,
    ]
];
