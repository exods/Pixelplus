BX.ready(function () {
    let button = document.querySelector('[data-table]');
    button.addEventListener('click', showTable);

    /**
     * тут контроллер модуля, он отдаёт объект, можно реализовать запрос данных через него и строить DOM :)
     * */

    function getData() {
        BX.ajax.runAction('sum:main.api.Ajax.getSum', {
            data: {}
        }).then(function (response) {
            console.log(response.data);
        }, function (response) {
            console.log(response.errors);
        });
    }
    /**
     * стучимся в файл и вставляем в дом
     * */
    function showTable() {
        let table = document.querySelector('.table_users');
        BX.ajax.insertToNode('/local/include/table.php', table)
        setTimeout(function () {
            button.innerText = 'Обновить данные';
        },1000)
    }
})
